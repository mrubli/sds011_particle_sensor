#!/usr/bin/env python3

import argparse
import csv
import datetime
import sys
import time
from sds011 import SDS011

def printlog(level, string):
    sys.stderr.write("%s: %s: %s\n" % (datetime.datetime.now(), level, string))

def processValues(pm25, pm10):
    print("{0}: PM2.5: {2} {1}, PM10: {3} {1}"
        .format(datetime.datetime.now().replace(microsecond=0), 'µg/m³', pm25, pm10))
    if csv_exporter is not None:
        csv_exporter.write_row(pm25=pm25, pm10=pm10)

def getCsvPath():
    if args.export_csv is None:
        return None
    if args.export_csv != '':
        return args.export_csv
    return datetime.datetime.now().strftime('%Y%m%d-%H%M%S.csv')

class CsvExporter:
    def __init__(self, path):
        self.__path = path
        self.__fh = open(path, 'w', newline='')
        self.__csv_writer = csv.DictWriter(self.__fh, fieldnames=['time', 'pm25', 'pm10'])
        self.__csv_writer.writeheader()
        self.__fh.flush()
    def __del__(self):
        self.__fh.close()
    def write_row(self, pm25, pm10):
        self.__csv_writer.writerow({
            'time': datetime.datetime.now().replace(microsecond=0).isoformat(),
            'pm25': pm25,
            'pm10': pm10 }
        )
        self.__fh.flush()


# Argument parsing
parser = argparse.ArgumentParser(description='Monitor particulate matter using SDS011 sensor')
parser.add_argument(
    '--port', '-p',
    help="serial port, e.g. /dev/ttyUSB0 or COM3 (default: %(default)s)",
    default="/dev/ttyUSB0",
)
parser.add_argument(
    '--debug',
    '-d',
    help="debug level, lower values are more verbose (range: 10..50, 0 = off, default: %(default)s)",
    type=int,
    default=0,
    metavar="L",
)
parser.add_argument(
    '--export-csv', '-e',
    help="export to CSV file (default: off)",
    default=None,
    const='',
    nargs='?',
)
parser.add_argument(
    '--interval', '-i',
    help="measurement interval in minutes (range: 0..30, 0 = continuous, default: %(default)s)",
    type=int,
    default=0,
    metavar="MIN",
)
parser.add_argument(
    '--cycles', '-c',
    help="number of test cycles (0 = infinite, default: %(default)s)",
    type=int,
    default=0,
    metavar="N",
)
parser.add_argument(
    '--timeout', '-t',
    help="timeout of serial line readings in seconds (default: %(default)s)",
    type=int,
    default=2,
    metavar="SEC",
)
parser.add_argument(
    '--no-exit-reset', '-R',
    help="don't reset the sensor on exit",
    action="store_true"
)
parser.add_argument(
    '--exit-sleep', '-s',
    help="put the sensor into sleep mode on exit (implies --no-exit-reset)",
    action="store_true"
)
args = parser.parse_args()

# Set up debugging
if args.debug > 0:
    # Activate simple logging
    import logging
    import logging.handlers
    logger = logging.getLogger()
    # Available levels are the well known logging.INFO, logging.WARNING and so forth.
    # Between INFO (20) and DEBUG (10) are fine grained messages with levels 14, 16 and 18.
    logger.setLevel(args.debug)

# Create a sensor instance
try:
    print("Initializing sensor on {0} ...".format(args.port))
    sensor = SDS011(args.port, timeout=args.timeout)
except Exception as e:
    print("ERROR:", e)
    sys.exit(1)
sensor.reset()
print("Querying sensor ...")
print("  Device ID:             0x{0}".format(sensor.device_id))
print("  Device firmware:       {0}".format(sensor.firmware))
print("  Measurement interval:  {0}".format(sensor.dutycycle if sensor.dutycycle != 0 else "continuous"))
print("  Work state:            {0}".format(sensor.workstate))
print("  Reporting mode:        {0}".format(sensor.reportmode))
print()
print("Performing {0}measurements {1}.{2}"
        .format("%d " % args.cycles if args.cycles > 0 else "",
                "in intervals of %d minute(s)" % args.interval if args.interval > 0 else "continuously",
                " Press Ctrl+C to cancel." if args.cycles > 0 else ""))

# Set up CSV exporter if requested
csv_path = getCsvPath()
csv_exporter = None
if csv_path is not None:
    print("Exporting measurement data to '{0}'.".format(csv_path))
    csv_exporter = CsvExporter(csv_path)

# Take measurements
try:
    sensor.workstate = SDS011.WorkStates.Measuring
    sensor.dutycycle = args.interval

    cycle = 0
    while cycle < args.cycles or args.cycles <= 0:
        cycle += 1
        if args.cycles > 0:
            print("Measurement {0} of {1}:".format(cycle, args.cycles))
        last = time.time()
        while True:
            values = sensor.get_values()
            if values is not None:
                pm10, pm25 = values
                processValues(pm25=pm25, pm10=pm10)
                break
    print("\nMeasurements completed.")
except KeyboardInterrupt:
    print("\nInterrupted.")
except Exception as e:
    print("ERROR:", e)

# Clean up
if args.exit_sleep:
    print("Putting sensor into sleep mode.")
    sensor.workstate = SDS011.WorkStates.Sleeping
elif not args.no_exit_reset:
    print("Resetting sensor.")
    sensor.reset()
sensor = None
