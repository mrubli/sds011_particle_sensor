# Some ways to run this script:
# - Non-interactive, console, Unicode-rendered charts:
#     julia pm.jl
# - Interactive, console, charts as pop-up windows:
#     julia -i pm.jl
# - Open in Visual Studio Code with the Julia extension
# In all of these cases you'll have to install the required packages first. Type a right bracket
# in the REPL and then this:
#   add CSV Plots PyPlot UnicodePlots

using CSV
using Plots
using DataFrames
using Dates

# Read the CSV file. For clarity define the row indices as constants.
const time = 1
const pm25 = 2
const pm10 = 3
data = CSV.read("../pm.csv", DataFrame, comment="#")

# Pick a plotting engine
if isinteractive()
    pyplot()
else
    unicodeplots()
end

# Plot the data
values = hcat(
    data[:,pm25],
    data[:,pm10],
)
display(plot(data[:,time], values,
    title = "PM measurements",
    size = (600, 600),
    xlabel = "time",
    xtickfontrotation = 60,
    ylabel = "µg/m³",
    ylims = (0,Inf),
    label = [ "PM2.5" "PM10" ],
    seriescolor = [ :gray :black ],
    # sadly xformatter doesn't seem to work for a time axis
))
